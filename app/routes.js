(function (angular) {
    'use strict';


    angular.module('mypos').config(function ($stateProvider, $urlRouterProvider) {

        const layoutState = {
            abstract: true,
            name: 'layout',
            url: '/layout',
            component: 'layout'
        };

        const menuState = {
            name: 'layout.menu',
            url: '/menu',
            component: 'menu'
        };

        const basketState = {
            name: 'layout.basket',
            url: '/basket',
            component: 'basket'
        };

        const kitchenState = {
            name: 'layout.kitchen',
            url: '/kitchen',
            component: 'kitchen'
        };

        $stateProvider.state(layoutState);
        $stateProvider.state(menuState);
        $stateProvider.state(basketState);
        $stateProvider.state(kitchenState);

        $urlRouterProvider.otherwise('/layout/menu');
    });

})(window.angular);
