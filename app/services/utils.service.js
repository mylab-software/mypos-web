(function (angular) {
    'use strict';


    angular.module('mypos').service('Utils', [function () {
        this.disableButtons = function () {
            $('.btn').addClass('disabled');
        };

        this.enableButtons = function () {
            $('.btn').removeClass('disabled');
        }
    }]);

})(window.angular);
