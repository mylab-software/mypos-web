(function (angular) {
    'use strict';

    angular.module('mypos').service('ItemService', ['$http', '$log', 'API_v1',
        function ($http, $log, API_v1) {
            let BASE_URL_v1 = API_v1 + 'item/';

            this.getItem = function (id) {
                return $http.get(BASE_URL_v1 + id + '/');
            };

            this.getAll = function (limit, offset) {
                let params = {};
                if (limit) {
                    params.limit = limit;
                }
                if (offset) {
                    params.offset = offset;
                }
                return $http.get(BASE_URL_v1, {
                    params: params
                });
            };

        }]);

})(window.angular);
