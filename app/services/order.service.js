(function (angular) {
    'use strict';

    angular.module('mypos').service('OrderService', ['$http', '$log', 'API_v1',
        function ($http, $log, API_v1) {
            let BASE_URL_v1 = API_v1 + 'order/';

            this.getOrder = function (id) {
                return $http.get(BASE_URL_v1 + id + '/');
            };

            this.getAll = function (status, limit, offset) {
                let params = {};
                if (limit) {
                    params.limit = limit;
                }
                if (offset) {
                    params.offset = offset;
                }
                if (status) {
                    params.status = status;
                }
                return $http.get(BASE_URL_v1, {
                    params: params
                });
            };

            this.addOrder = function (itemId) {
                return $http.post(BASE_URL_v1, {
                    item_id: itemId,
                });
            };

            this.changeStatus = function (id, itemId, status) {
                return $http.put(BASE_URL_v1 + id + '/', {
                    id: id,
                    item_id: itemId,
                    status: status,
                });
            };

        }]);

})(window.angular);


