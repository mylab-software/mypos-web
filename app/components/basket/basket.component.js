(function (angular) {
    'use strict';

    function BasketCtrl($scope, $log, OrderService) {
        $scope.orders = [];

        $scope.refreshOrders = function () {
            OrderService.getAll().then(
                function (response) {
                    $log.log('response: ', response);
                    $scope.orders = response.data.bdata.results;
                },
                function (errors) {
                    $log.log('errors: ', errors);
                }
            );
        };

        $scope.refreshOrders();
    }


    angular.module('mypos').component('basket', {
        templateUrl: 'app/components/basket/basket.html',
        controller: ['$scope', '$log', 'OrderService', BasketCtrl]
    });

})(window.angular);
