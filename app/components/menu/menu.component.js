(function (angular) {
    'use strict';

    function MenuCtrl($scope, $log, ItemService) {
        $scope.items = [];

        $scope.refreshMenu = function () {
            ItemService.getAll().then(
                function (response) {
                    $log.log('response: ', response);
                    $scope.items = response.data.bdata.results;
                },
                function (errors) {
                    $log.log('errors: ', errors);
                }
            );
        };

        $scope.refreshMenu();
    }

    angular.module('mypos').component('menu', {
        templateUrl: 'app/components/menu/menu.html',
        controller: ['$scope', '$log', 'ItemService', MenuCtrl]
    });
})(window.angular);
