(function (angular) {

    'use strict';

    function KitchenCtrl($scope, $log, OrderService) {
        $scope.orders = [];

        $scope.refreshOrders = function () {
            OrderService.getAll().then(
                function (response) {
                    $log.log('response: ', response);
                    $scope.orders = response.data.bdata.results;
                },
                function (errors) {
                    $log.log('errors: ', errors);
                }
            );
        };

        $scope.checkAction = function (action, status) {
            if (action === 'cooking') {
                return status === 'waiting';
            } else if (action === 'ready') {
                return status === 'cooking';
            } else if (action === 'canceled') {
                return ['waiting', 'cooking'].includes(status);
            }
            return false;
        };

        $scope.refreshOrders();
    }

    angular.module('mypos').component('kitchen', {
        templateUrl: 'app/components/kitchen/kitchen.html',
        controller: ['$scope', '$log', 'OrderService', KitchenCtrl]
    });

})(window.angular);
