(function (angular) {
    'use strict';

    function OrderStatusCtrl($scope) {
        let self = this;
    }


    angular.module('mypos').component('orderStatus', {
        templateUrl: 'app/components/order-status/order-status.html',
        controller: ['$scope', OrderStatusCtrl],
        bindings: {
            status: '@'
        }
    });

})(window.angular);
