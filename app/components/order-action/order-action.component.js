(function (angular) {
    'use strict';

    function OrderActionCtrl($scope, OrderService) {
        $scope.changeStatus = function (orderId, itemId, newStatus) {
            OrderService.changeStatus(orderId, itemId, newStatus);
        };
    }

    angular.module('mypos').component('orderAction', {
        templateUrl: 'app/components/order-action/order-action.html',
        controller: ['$scope', 'OrderService', OrderActionCtrl],
        bindings: {
            action: '@',
            order: '<'
        }
    });

})(window.angular);
