(function (angular) {
    'use strict';

    function ItemCtrl($scope, $attrs, OrderService) {
        $scope.addOrder = function (itemId) {
            OrderService.addOrder(itemId);
        }
    }


    angular.module('mypos').component('item', {
        templateUrl: 'app/components/item/item.html',
        controller: ['$scope', '$attrs', 'OrderService', ItemCtrl],
        bindings: {
            data: '<'
        }
    });

})(window.angular);
