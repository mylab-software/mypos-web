(function (angular) {

    'use strict';

    function LayoutCtrl($scope) {

    }

    angular.module('mypos').component('layout', {
        templateUrl: 'app/components/layout/layout.html',
        controller: ['$scope', LayoutCtrl]
    });

})(window.angular);
