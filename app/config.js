(function (angular) {
    'use strict';

    angular.module('mypos').constant('API_v1', 'http://localhost:8000/api/v1/');

    angular.module('mypos').factory('ReqInterceptor', ['$q', 'Utils', function ($q, Utils) {
        return {
            'request': function (config) {
                Utils.disableButtons();
                return config;
            },
            'requestError': function (rejection) {
                Utils.enableButtons();
                return $q.reject(rejection);
            },
            'response': function (response) {
                Utils.enableButtons();
                return response;
            },
            'responseError': function (rejection) {
                Utils.enableButtons();
                return $q.reject(rejection);
            }
        };
    }]);

    angular.module('mypos').config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('ReqInterceptor');
    }]);
})(window.angular);
