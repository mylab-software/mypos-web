Test work web client of BurgerHouse
==================================

Run server
-----------

- `cd` into root folder
- run commnad: `python -m http.server 7800`
- running server on: http://localhost:7800/


Usage
-----------

- menu url: *BASE_URI/layout/menu* (will be opend by default)
- kitchen url: *BASE_URI/layout/kitchen*
- basket url: *BASE_URI/layout/basket*
